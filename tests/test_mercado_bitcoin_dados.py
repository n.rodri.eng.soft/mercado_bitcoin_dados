from datetime import datetime, date
from pandas._testing import assert_frame_equal
from mercado_bitcoin_dados import __version__
import pandas as pd
import pytest
from mercado_bitcoin_dados import (
    CryptoAsset,
    Ticker,
    DayTradeSummary,
    HistoricData,
    OrderBook,
    MercadoBitcoinData)
from enum import Enum

request_mock = 'requests.Response.json'
def test_version():
    assert __version__ == '0.1.0'

def test_crypto_asset():
    crypto_asset = CryptoAsset.BTC
    assert isinstance(crypto_asset, Enum)
    assert crypto_asset.code == 'BTC'
    assert crypto_asset.description == 'Bitcoin'
    
     
def test_creation_ticker_object():
    ticker_dict = { 'high': 1,
        'open':1,
        'low': 2,
        'vol': 3,
        'last': 4,
        'buy': 5,
        'sell': 6,
        'date': 7}
    ticker = Ticker(**ticker_dict)
    assert ticker.high == 1
    assert ticker.low == 2
    assert ticker.vol == 3
    assert ticker.last == 4
    assert ticker.buy == 5
    assert ticker.sell == 6
    assert ticker.date == 7



def test_api_ticker():
    api_client = MercadoBitcoinData()
    response=api_client.get_response_ticker(CryptoAsset.BTC)
    assert response.ok
    

def test_creation_of_ticker(mocker):
    api_client = MercadoBitcoinData()
    json_reponse = {
        'ticker': {
            'high': 14481.47000000,
            'low': 13706.00002000,
            'open': 13706.00002000,
            'vol': 443.73564488,
            'last': 14447.01000000,
            'buy': 14447.00100000,
            'sell': 14447.01000000,
            'date': 1502977646
            }
        }
    def mock_load(self):
        return json_reponse

    mocker.patch(
        request_mock,
        mock_load
        )
    ticker=api_client.get_ticker(CryptoAsset.BTC)
    assert json_reponse['ticker']['last'] == ticker.last

def test_get_orderbook(mocker):
    api_client = MercadoBitcoinData()
    response=api_client.get_response_orderbook(CryptoAsset.BTC)
    assert response.ok

def test_get_asks(
        mocker, 
        orderbook_response,
        target_asks,
        target_bids):
    api_client = MercadoBitcoinData()
    def mock_load(self):
        return orderbook_response
    mocker.patch(
        request_mock,
        mock_load
        )
    orderbook=api_client.get_orderbook(CryptoAsset.BTC)
    df_asks = orderbook.asks
    df_bids = orderbook.bids
    
    assert_frame_equal(df_asks, target_asks)
    assert_frame_equal(df_bids, target_bids)

def test_assert_exception_orderbook():
    asks = pd.DataFrame({'no_price':[1,2,3],'no_size':[1,2,3]})
    bids = pd.DataFrame({'no_price':[1,2,3],'no_size':[1,2,3]})
    with pytest.raises(AssertionError):
        OrderBook([1,2,3],[1,2,3])
    with pytest.raises(AssertionError):
        OrderBook(asks,bids)

def test_get_trades_response(mocker):
    api_client = MercadoBitcoinData()
    trade_id = 5700
    response=api_client.get_response_trades_since_trade_id(CryptoAsset.BTC, trade_id)
    assert response.ok

def test_get_trades(mocker, 
        trades_response,
        target_trades):
    api_client = MercadoBitcoinData()
    def mock_load(self):
        return trades_response
    mocker.patch(
        request_mock,
        mock_load
        )
    trade_id = 5700
    trades = api_client.get_trades_since_trade_id(CryptoAsset.BTC , trade_id)
    assert_frame_equal(trades, target_trades)

def test_get_trades_response_from(mocker):
    api_client = MercadoBitcoinData()
    from_date  = datetime.fromisoformat('2020-01-01')
    from_timestamp = api_client._from_datetime_to_unix(from_date)
    response = api_client.get_response_trades_from(CryptoAsset.BTC, from_timestamp)
    assert response.ok

def test_get_trades_from(mocker, 
        trades_response,
        target_trades):
    api_client = MercadoBitcoinData()
    def mock_load(self):
        return trades_response
    mocker.patch(
        request_mock,
        mock_load
        )
    from_date  = datetime.fromisoformat('2020-01-01')
    trades = api_client.get_trades_from(CryptoAsset.BTC , from_date)
    assert_frame_equal(trades, target_trades)


def test_get_trades_response_from_to(mocker):
    api_client = MercadoBitcoinData()
    from_date  = datetime.fromisoformat('2020-01-01')
    to_date  = datetime.fromisoformat('2020-01-02')
    
    from_timestamp = api_client._from_datetime_to_unix(from_date)
    to_timestamp = api_client._from_datetime_to_unix(to_date)
    
    response = api_client.get_response_trades_from_to(
        CryptoAsset.BTC,
        from_timestamp,
        to_timestamp
        )
    assert response.ok

def test_get_trades_from_to(mocker, 
        trades_response,
        target_trades):
    api_client = MercadoBitcoinData()
    def mock_load(self):
        return trades_response
    mocker.patch(
        request_mock,
        mock_load
        )
    from_date  = datetime.fromisoformat('2020-01-01')
    to_date  = datetime.fromisoformat('2020-01-01')
    trades = api_client.get_trades_from_to(CryptoAsset.BTC , from_date,to_date )
    assert_frame_equal(trades, target_trades)

def test_get_day_summary():
    api_client = MercadoBitcoinData()
    day = date(2020, 8, 23)

    day_trade_sumary = api_client.get_day_summary(CryptoAsset.BTC ,day)
    assert isinstance(day_trade_sumary, DayTradeSummary)
    assert day_trade_sumary.date == day


def test_get_day_summary_response():
    api_client = MercadoBitcoinData()
    day = date(2020, 8, 23)
    response = api_client.get_response_day_summary(CryptoAsset.BTC ,day)
    assert response.ok

def test_create_historic_data(day_summary,historic_data_basic_target):
    historic_data = HistoricData.from_day_summary(day_summary)
    assert isinstance(historic_data , pd.DataFrame)
    assert_frame_equal(historic_data, historic_data_basic_target)


def test_get_historic_data(mocker, summarys , historic_data_target):
    api_client = MercadoBitcoinData()
    mocker.patch.object(
        api_client,
        'get_day_summary',
        side_effect = summarys
        )

    
    first_summary = summarys[0]
    last_summary = summarys[-1]
    from_date = first_summary.date
    to_date = last_summary.date
    historic_data = api_client.get_historic_data(CryptoAsset.BTC,from_date, to_date)
    assert_frame_equal(historic_data, historic_data_target)
    assert historic_data.get_day_summary(from_date) == first_summary
    assert historic_data.get_day_summary(to_date) == last_summary
    