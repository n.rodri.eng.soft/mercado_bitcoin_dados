import pytest
import pandas as pd
import numpy as np
from datetime import date
from dataclasses import asdict

from mercado_bitcoin_dados import DayTradeSummary

@pytest.fixture
def target_asks(orderbook_response):
    array = np.array(orderbook_response['asks'])
    asks_df = pd.DataFrame(array, columns = ['price','size'])
    return asks_df

@pytest.fixture
def target_bids(orderbook_response):
    array = np.array(orderbook_response['bids'])
    bids_df = pd.DataFrame(array, columns = ['price','size'])
    return bids_df

@pytest.fixture
def orderbook_response():
    json = {
            "asks": [
                [10410.00006000, 2.09190016],
                [10420.00000000, 0.00997000],
                [10488.99999000, 0.46634897]
            ],
            "bids": [
                [10405.38258000, 0.00181000],
                [10393.84180000, 0.08387000]
            ]
        }
    return json

@pytest.fixture
def trades_response():
    json = [
        {
            "tid": 5701,
            "date": 1373119254,
            "type": "buy",
            "price": 200,
            "amount": 0.2
        },
        {
            "tid": 5702,
            "date": 1373122679,
            "type": "sell",
            "price": 201,
            "amount": 0.187
        }
    ]
    return json

@pytest.fixture
def target_trades(trades_response):
    return pd.DataFrame.from_records(trades_response)

@pytest.fixture
def day_summary():
    return  DayTradeSummary(
        date(2020, 8, 23),
        262.99999,
        269.0,
        260.00002,
        269.0,
        7253.1336356785,
        27.11390588,
        28,
        267.5060416518087
    )

@pytest.fixture
def historic_data_basic_target(trades_response):
    day = date(2020, 8, 23)
    data = {
        'opening': 262.99999,
        'closing': 269.0,
        'lowest': 260.00002,
        'highest': 269.0,
        'volume': 7253.1336356785,
        'quantity': 27.11390588,
        'amount': 28,
        'avg_price': 267.5060416518087
    }
    return  pd.DataFrame.from_records(data, index= [day])

@pytest.fixture
def summarys():
    return  [ 
        DayTradeSummary(
            date(2020, 8, 23),
            262.99999,
            269.0,
            260.00002,
            269.0,
            7253.1336356785,
            27.11390588,
            28,
            267.5060416518087
            ), 
        DayTradeSummary(
            date(2020, 8, 24),
            262.99999,
            269.0,
            260.00002,
            269.0,
            7253.1336356785,
            27.11390588,
            28,
            267.5060416518087
            )]

@pytest.fixture
def historic_data_target(summarys):
    data = [asdict(summary) for summary in summarys ]

    return  pd.DataFrame.from_records(data, index= 'date')